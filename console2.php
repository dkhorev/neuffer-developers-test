<?php

use App\Format\Input\CsvInput;
use App\Format\Output\CsvOutput;
use App\Format\Output\ErrorLog;
use App\Math\Factories\MathFactory;

require_once 'vendor/autoload.php';

$shortopts = "a:f:";
$longopts  = array(
    "action:",
    "file:",
);

$options = getopt($shortopts, $longopts);

if(isset($options['a'])) {
    $action = $options['a'];
} elseif(isset($options['action'])) {
    $action = $options['action'];
} else {
    $action = "xyz";
}

if(isset($options['f'])) {
    $file = $options['f'];
} elseif(isset($options['file'])) {
    $file = $options['file'];
} else {
    $file = "notexists.csv";
}

// !!! refactored part starts here !!!
try {
    // todo this can be factory
    $data = (new CsvInput($file))->parse();

    $result = MathFactory::operation($action)->execute($data);

    // todo this can be factory
    (new CsvOutput('result.csv'))->save($result->data());

    // todo this can be factory
    (new ErrorLog('log.txt'))->save($result->errors());
} catch (Exception $e) {
    echo $e->getMessage();
}

