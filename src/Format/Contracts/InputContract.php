<?php

namespace App\Format\Contracts;

interface InputContract
{
    /**
     * Read input and return data array
     *
     * @return array
     */
    public function parse(): array;
}
