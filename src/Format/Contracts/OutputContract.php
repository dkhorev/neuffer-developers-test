<?php

namespace App\Format\Contracts;

interface OutputContract
{
    /**
     * Store data
     *
     * @param array $data
     *
     * @return bool
     */
    public function save(array $data): bool;
}
