<?php

namespace App\Format\Input;

use App\Format\Contracts\InputContract;
use Exception;

/**
 * Class CsvInput
 *
 * @package App\Format\Input
 */
class CsvInput implements InputContract
{
    /**
     * @var string
     */
    protected $file;

    /**
     * @var bool|resource
     */
    private $handle;

    /**
     * CsvInput constructor.
     *
     * @param string $file
     *
     * @throws Exception
     */
    public function __construct(string $file)
    {
        $this->file = $file;

        try {
            $this->handle = fopen($this->file, 'r');
        } catch (Exception $e) {
        }

        if (!$this->handle) {
            throw new Exception('Error opening input file');
        }
    }

    /**
     * Read input and return data array
     *
     * @return array
     */
    public function parse(): array
    {
        $result = [];

        while (($data = fgetcsv($this->handle, 0, ';')) !== false) {
            $result[] = array_map(function ($number) {
                return preg_replace('/([^\-0-9\.,])/i', '', $number);
            }, $data);
        }

        fclose($this->handle);

        return $result;
    }
}
