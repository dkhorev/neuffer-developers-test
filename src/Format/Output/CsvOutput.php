<?php

namespace App\Format\Output;

use App\Format\Contracts\OutputContract;
use Exception;

/**
 * Class CsvOutput
 *
 * @package App\Format\Output
 */
class CsvOutput implements OutputContract
{
    /**
     * @var string
     */
    protected $file;

    /**
     * @var bool|resource
     */
    private $handle;

    /**
     * CsvInput constructor.
     *
     * @param string $file
     *
     * @throws Exception
     */
    public function __construct(string $file)
    {
        $this->file = $file;

        try {
            $this->handle = fopen($this->file, "w");
        } catch (Exception $e) {
            throw new Exception('Error reading output file: ' . $e->getMessage());
        }
    }

    /**
     * Store data
     *
     * @param array $data
     *
     * @return bool
     */
    public function save(array $data): bool
    {
        array_map(function ($line) {
            fputcsv($this->handle, $line, ';');
        }, $data);

        return fclose($this->handle);
    }
}
