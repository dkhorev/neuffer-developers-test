<?php

namespace App\Format\Output;

use App\Format\Contracts\OutputContract;
use Exception;

/**
 * Class ErrorLog
 *
 * @package App\Format\Output
 */
class ErrorLog implements OutputContract
{
    /**
     * @var string
     */
    protected $file;

    /**
     * @var bool|resource
     */
    private $handle;

    /**
     * ErrorLog constructor.
     *
     * @param string $file
     *
     * @throws Exception
     */
    public function __construct(string $file)
    {
        $this->file = $file;

        try {
            $this->handle = fopen($this->file, "w");
        } catch (Exception $e) {
            throw new Exception('Error reading output file: ' . $e->getMessage());
        }
    }

    /**
     * Store data
     *
     * @param array $data
     *
     * @return bool
     */
    public function save(array $data): bool
    {
        array_map(function ($line) {
            fwrite($this->handle, $this->formatLine($line));
        }, $data);

        return fclose($this->handle);
    }

    /**
     * @param $line
     *
     * @return string
     */
    private function formatLine($line)
    {
        list ($a, $b) = $line;

        return "numbers {$a} and {$b} are wrong" . PHP_EOL;
    }
}
