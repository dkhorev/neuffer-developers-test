<?php

namespace App\Math\Contracts;

interface InputRuleContract
{
    /**
     * Check if rule validation passes
     *
     * @param array $line
     *
     * @return bool
     */
    public function passes(array $line): bool;
}
