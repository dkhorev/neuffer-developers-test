<?php

namespace App\Math\Contracts;

/**
 * Interface MathFactoryContract
 *
 * @package App\Math\Contracts
 */
interface MathFactoryContract
{
    /**
     * Create instance of operation
     *
     * @param string $operation
     *
     * @return OperationContract
     */
    public static function operation(string $operation): OperationContract;
}
