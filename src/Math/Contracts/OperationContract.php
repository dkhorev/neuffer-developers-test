<?php

namespace App\Math\Contracts;

interface OperationContract
{
    /**
     * Execute operation with all the rules
     *
     * @param array $data
     *
     * @return OperationResultContract
     */
    public function execute(array $data): OperationResultContract;
}
