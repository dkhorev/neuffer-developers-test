<?php

namespace App\Math\Contracts;

interface OperationResultContract
{
    /**
     * Correct results
     * ex: [1,2,3,4] => 1,2,3 - args, 4 = result of operation
     *
     * @return array
     */
    public function data(): array;

    /**
     * Bad results
     * ex: [1,-1,0] => 1,-1 - args, 0 = result of operation
     *
     * @return array
     */
    public function errors(): array;

    /**
     * Add new line to resulting data stack
     *
     * @param array $data
     *
     * @return OperationResultContract
     */
    public function addData(array $data): OperationResultContract;

    /**
     * Add new line to error data stack
     *
     * @param array $data
     *
     * @return OperationResultContract
     */
    public function addError(array $data): OperationResultContract;
}
