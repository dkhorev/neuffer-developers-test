<?php

namespace App\Math\Contracts;

interface ResultRuleContract
{
    /**
     * Check if rule validation passes
     *
     * @param $value
     *
     * @return bool
     */
    public function passes($value): bool;
}
