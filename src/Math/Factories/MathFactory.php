<?php

namespace App\Math\Factories;

use App\Math\Contracts\MathFactoryContract;
use App\Math\Contracts\OperationContract;
use App\Math\Operations\OperationResult;
use PHPUnit\Util\Exception;

/**
 * Class MathFactory
 *
 * @package App\Math\Factories
 */
class MathFactory implements MathFactoryContract
{
    const OPERATION_CLASS_PATH = 'App\\Math\\Operations\\';

    /**
     * Create instance of operation
     *
     * @param string $operation
     *
     * @return OperationContract
     */
    public static function operation(string $operation): OperationContract
    {
        $class = static::OPERATION_CLASS_PATH . ucfirst($operation);
        if (!class_exists($class)) {
            throw new Exception('Operation does not exist: ' . $operation);
        }

        return new $class(new OperationResult());
    }
}
