<?php

namespace App\Math\Operations;

use App\Math\Contracts\InputRuleContract;
use App\Math\Contracts\OperationContract;
use App\Math\Contracts\OperationResultContract;
use App\Math\Contracts\ResultRuleContract;
use App\Math\Rules\Result\NoNegative;
use App\Math\Rules\Validation\ValidArguments;

/**
 * Class AbstractOperation
 *
 * @package App\Math\Operations
 */
abstract class AbstractOperation implements OperationContract
{
    /**
     * @var OperationResultContract
     */
    protected $operationResult;

    /**
     * Array of input validation rules
     *
     * @var InputRuleContract[]
     */
    protected $inputRules = [];

    /**
     * Array of result validation rules
     *
     * @var ResultRuleContract[]
     */
    protected $resultRules = [];

    /**
     * Performs calculation
     *
     * @param array $line
     *
     * @return int|float
     */
    abstract protected function operation(array $line);

    /**
     * AbstractOperation constructor.
     *
     * @param OperationResultContract $resultStorage
     */
    public function __construct(OperationResultContract $resultStorage)
    {
        $this->operationResult = $resultStorage;

        // default input validation rules
        $this->inputRules[] = new ValidArguments();

        // default result validation rules
        $this->resultRules[] = new NoNegative();
    }

    /**
     * Execute operation with all the rules
     *
     * @param array $data
     *
     * @return OperationResultContract
     */
    public function execute(array $data): OperationResultContract
    {
        array_map(function ($line) {
            if ($this->inputRulesPass($line)) {
                $result = $this->operation($line);

                if ($this->resultRulesPass($result)) {
                    $this->operationResult->addData(array_merge($line, [$result]));

                    return;
                }
            }

            $this->operationResult->addError($line);

        }, $data);

        return $this->operationResult;
    }

    /**
     * @param array $line
     *
     * @return bool
     */
    private function inputRulesPass(array $line): bool
    {
        $result = true;

        array_map(function (InputRuleContract $rule) use ($line, &$result) {
            if (!$rule->passes($line)) {
                $result = false;
            }
        }, $this->inputRules);

        return $result;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    private function resultRulesPass($value): bool
    {
        $result = true;

        array_map(function (ResultRuleContract $rule) use ($value, &$result) {
            if (!$rule->passes($value)) {
                $result = false;
            }
        }, $this->resultRules);

        return $result;
    }
}
