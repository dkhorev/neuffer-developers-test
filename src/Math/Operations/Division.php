<?php

namespace App\Math\Operations;

use App\Math\Contracts\OperationResultContract;
use App\Math\Rules\Validation\NoDivisionByZero;

/**
 * Class Division
 *
 * @package App\Math\Operations
 */
class Division extends AbstractOperation
{
    public function __construct(OperationResultContract $resultStorage)
    {
        parent::__construct($resultStorage);

        // operation specific input validation rules
        $this->inputRules[] = new NoDivisionByZero();
    }

    /**
     * Performs calculation
     *
     * @param $line
     *
     * @return int|float
     */
    protected function operation(array $line)
    {
        list($a, $b) = $line;

        return $a / $b;
    }
}
