<?php

namespace App\Math\Operations;

/**
 * Class Minus
 *
 * @package App\Math\Operations
 */
class Minus extends AbstractOperation
{
    /**
     * Performs calculation
     *
     * @param $line
     *
     * @return int|float
     */
    protected function operation(array $line)
    {
        list($a, $b) = $line;

        return $a - $b;
    }
}
