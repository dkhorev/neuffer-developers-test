<?php

namespace App\Math\Operations;

/**
 * Class Multiply
 *
 * @package App\Math\Operations
 */
class Multiply extends AbstractOperation
{
    /**
     * Performs calculation
     *
     * @param $line
     *
     * @return int|float
     */
    protected function operation(array $line)
    {
        list($a, $b) = $line;

        return $a * $b;
    }
}
