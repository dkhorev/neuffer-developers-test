<?php

namespace App\Math\Operations;

use App\Math\Contracts\OperationResultContract;

/**
 * Results storage
 *
 * Class OperationResult
 *
 * @package App\Math\Operations
 */
class OperationResult implements OperationResultContract
{
    /**
     * Result storage
     * @var array
     */
    private $data = [];

    /**
     * Errors storage
     * @var array
     */
    private $errors = [];

    /**
     * Correct results
     * ex: [1,2,3,4] => 1,2,3 - args, 4 = result of operation
     *
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }

    /**
     * Bad results
     * ex: [1,-1,0] => 1,-1 - args, 0 = result of operation
     *
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * Add new line to resulting data stack
     *
     * @param array $data
     *
     * @return OperationResultContract
     */
    public function addData(array $data): OperationResultContract
    {
        $this->data[] = $data;

        return $this;
    }

    /**
     * Add new line to error data stack
     *
     * @param array $data
     *
     * @return OperationResultContract
     */
    public function addError(array $data): OperationResultContract
    {
        $this->errors[] = $data;

        return $this;
    }
}
