<?php

namespace App\Math\Operations;

/**
 * Class Plus
 *
 * @package App\Math\Operations
 */
class Plus extends AbstractOperation
{
    /**
     * Performs calculation
     *
     * @param $line
     *
     * @return int|float
     */
    protected function operation(array $line)
    {
        list($a, $b) = $line;

        return $a + $b;
    }
}
