<?php

namespace App\Math\Rules\Result;

use App\Math\Contracts\ResultRuleContract;

/**
 * Class NoNegative
 *
 * @package App\Math\Rules\Result
 */
class NoNegative implements ResultRuleContract
{
    /**
     * Check if rule validation passes
     *
     * @param $value
     *
     * @return bool
     */
    public function passes($value): bool
    {
        return $value >= 0;
    }
}
