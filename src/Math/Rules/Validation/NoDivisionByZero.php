<?php

namespace App\Math\Rules\Validation;

use App\Math\Contracts\InputRuleContract;

/**
 * Class NoDivisionByZero
 *
 * @package App\Math\Rules\Validation
 */
class NoDivisionByZero implements InputRuleContract
{
    /**
     * Check if rule validation passes
     *
     * @param array $line
     *
     * @return bool
     */
    public function passes(array $line): bool
    {
        list(, $b) = $line;

        return (int)$b !== 0 && (float) $b !== 0;
    }
}
