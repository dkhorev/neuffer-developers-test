<?php

namespace App\Math\Rules\Validation;

use App\Math\Contracts\InputRuleContract;

/**
 * Class ValidArguments
 *
 * @package App\Math\Rules\Validation
 */
class ValidArguments implements InputRuleContract
{
    /**
     * Check if rule validation passes
     *
     * @param array $line
     *
     * @return bool
     */
    public function passes(array $line): bool
    {
        list($a, $b) = $line;

        return is_numeric($a) && is_numeric($b);
    }
}
