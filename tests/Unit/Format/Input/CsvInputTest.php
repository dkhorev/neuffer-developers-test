<?php

namespace Tests\Unit\Format\Input;

use App\Format\Contracts\InputContract;
use App\Format\Input\CsvInput;
use Exception;
use Tests\TestCase;

class CsvInputTest extends TestCase
{
    const INPUT_FILE_PATH = __DIR__ . DIRECTORY_SEPARATOR .'input.csv';

    /** @test */
    public function testInstantiate()
    {
        /** @var InputContract $input */
        $input = new CsvInput(static::INPUT_FILE_PATH);

        $this->assertInstanceOf(CsvInput::class, $input);
        $this->assertInstanceOf(InputContract::class, $input);
    }

    /** @test */
    public function testParse()
    {
        /** @var InputContract $input */
        $input = new CsvInput(static::INPUT_FILE_PATH);

        // act
        $result = $input->parse();

        // assert
        $this->assertEquals([
            [-97, 90],
            [72, -58],
            [-1, 10],
            [3, 0],
            [0.1,1.1],
        ], $result);
    }

    /** @test */
    public function testInvalidFileException()
    {
        // pre assert
        $this->expectException(Exception::class);

        new CsvInput('somebadfilename');
    }
}
