<?php

namespace Tests\Unit\Format\Output;

use App\Format\Contracts\OutputContract;
use App\Format\Output\CsvOutput;
use Exception;
use Tests\TestCase;

class CsvOutputTest extends TestCase
{
    const OUTPUT_FILE_PATH = __DIR__ . DIRECTORY_SEPARATOR . 'result.csv';

    /** @test */
    public function testInstantiate()
    {
        /** @var OutputContract $output */
        $output = new CsvOutput(static::OUTPUT_FILE_PATH);

        $this->assertInstanceOf(CsvOutput::class, $output);
        $this->assertInstanceOf(OutputContract::class, $output);
    }

    /** @test */
    public function testSaves()
    {
        /** @var OutputContract $output */
        $output = new CsvOutput(static::OUTPUT_FILE_PATH);

        // act
        $output->save([
            [-97, 90, 1],
            [72, -58, 1],
            [-1, 10, 1],
            [3, 0, 1],
            [0.1, 1.1, 1],
        ]);

        // assert - read result file
        $handle = fopen(static::OUTPUT_FILE_PATH, 'r');
        $data = fread($handle, 100000);
        $this->assertEquals("-97;90;1\n72;-58;1\n-1;10;1\n3;0;1\n0.1;1.1;1\n", $data);
        fclose($handle);
    }

    /** @test */
    public function testInvalidFileException()
    {
        // pre assert
        $this->expectException(Exception::class);

        new CsvOutput('');
    }
}
