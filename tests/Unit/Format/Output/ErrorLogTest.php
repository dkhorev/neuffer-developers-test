<?php

namespace Tests\Unit\Format\Output;

use App\Format\Contracts\OutputContract;
use App\Format\Output\CsvOutput;
use App\Format\Output\ErrorLog;
use Exception;
use Tests\TestCase;

class ErrorLogTest extends TestCase
{
    const OUTPUT_FILE_PATH = __DIR__ . DIRECTORY_SEPARATOR . 'log.txt';

    /** @test */
    public function testInstantiate()
    {
        /** @var OutputContract $output */
        $output = new ErrorLog(static::OUTPUT_FILE_PATH);

        $this->assertInstanceOf(ErrorLog::class, $output);
        $this->assertInstanceOf(OutputContract::class, $output);
    }

    /** @test */
    public function testSaves()
    {
        /** @var OutputContract $output */
        $output = new ErrorLog(static::OUTPUT_FILE_PATH);

        // act
        $output->save([
            [-97, 90],
            [72, -58],
            [-1, 10],
            [3, 0],
            [0.1, 1.1],
        ]);

        // assert - read result file
        $data = file(static::OUTPUT_FILE_PATH);
        $this->assertEquals([
            "numbers -97 and 90 are wrong\r\n",
            "numbers 72 and -58 are wrong\r\n",
            "numbers -1 and 10 are wrong\r\n",
            "numbers 3 and 0 are wrong\r\n",
            "numbers 0.1 and 1.1 are wrong\r\n",
        ], $data);
    }

    /** @test */
    public function testInvalidFileException()
    {
        // pre assert
        $this->expectException(Exception::class);

        new CsvOutput('');
    }
}
