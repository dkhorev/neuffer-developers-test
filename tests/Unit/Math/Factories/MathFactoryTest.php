<?php

namespace Unit\Math\Factories;

use App\Math\Contracts\MathFactoryContract;
use App\Math\Contracts\OperationContract;
use App\Math\Factories\MathFactory;
use App\Math\Operations\Division;
use App\Math\Operations\Minus;
use App\Math\Operations\Multiply;
use App\Math\Operations\Plus;
use Exception;
use Tests\TestCase;

class MathFactoryTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        $factory = new MathFactory();

        $this->assertInstanceOf(MathFactory::class, $factory);
        $this->assertInstanceOf(MathFactoryContract::class, $factory);
    }

    /** @test */
    public function testConstructsPlusOperation()
    {
        // act
        $operation = MathFactory::operation('plus');

        // assert
        $this->assertInstanceOf(Plus::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }

    /** @test */
    public function testConstructsMinusOperation()
    {
        // act
        $operation = MathFactory::operation('minus');

        // assert
        $this->assertInstanceOf(Minus::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }
    
    /** @test */
    public function testConstructsMultiplyOperation()
    {
        // act
        $operation = MathFactory::operation('multiply');

        // assert
        $this->assertInstanceOf(Multiply::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }

    /** @test */
    public function testConstructsDivisionOperation()
    {
        // act
        $operation = MathFactory::operation('division');

        // assert
        $this->assertInstanceOf(Division::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }

    /** @test */
    public function testThrowOnInvalidOperation()
    {
        // pre assert
        $this->expectException(Exception::class);

        // act
        MathFactory::operation('badoperationname');
    }
}
