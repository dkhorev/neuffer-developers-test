<?php

namespace Tests\Unit\Math\Operations;

use App\Math\Contracts\OperationContract;
use App\Math\Operations\Division;
use App\Math\Operations\Minus;
use App\Math\Operations\Multiply;
use App\Math\Operations\OperationResult;
use App\Math\Operations\Plus;
use Tests\TestCase;

class AbstractOperationTest extends TestCase
{
    public function validInputCheckProvider()
    {
        return [
            [Plus::class],
            [Minus::class],
            [Multiply::class],
            [Division::class],
        ];
    }

    /**
     * @test
     * @dataProvider validInputCheckProvider
     *
     * @param $class
     */
    public function testValidatesInput($class)
    {
        /** @var OperationContract $operation */
        $operation = new $class(new OperationResult());

        // act
        $result = $operation->execute([
            ['', 2],
            ['', null],
            [10, null],
            [10, ''],
        ]);

        // assert
        $this->assertEquals([], $result->data());
        $this->assertEquals([
            ['', 2],
            ['', null],
            [10, null],
            [10, ''],
        ], $result->errors());
    }

    public function resultsCheckProvider()
    {
        return [
            [Plus::class, [
                [0, -1],
                [1, -2],
                [-1, -1],
            ]],
            [Minus::class, [
                [0, 2],
                [1, 3],
                [-1, 5],
            ]],
            [Multiply::class, [
                [1, -2],
                [-1, 100],
                [-1, 10],
                [-1, 5],
            ]],
            [Division::class, [
                [1, -2],
                [-1, 100],
                [-1, 10],
                [-1, 5],
            ]],
        ];
    }

    /**
     * @test
     * @dataProvider resultsCheckProvider
     *
     * @param $class
     * @param $data
     */
    public function testNoNegativeResults($class, $data)
    {
        /** @var OperationContract $operation */
        $operation = new $class(new OperationResult());

        // act
        $result = $operation->execute($data);

        // assert
        $this->assertEquals([], $result->data());
        $this->assertEquals($data, $result->errors());
    }

    /** @test */
    public function testNoDivisionByZeroRule()
    {
        /** @var OperationContract $operation */
        $operation = new Division(new OperationResult());

        // act
        $result = $operation->execute([
            [1, 0],
            [2, -0],
            [10, 0],
            [200, -0],
        ]);

        // assert
        $this->assertEquals([], $result->data());
        $this->assertEquals([
            [1, 0],
            [2, -0],
            [10, 0],
            [200, -0],
        ], $result->errors());
    }
}
