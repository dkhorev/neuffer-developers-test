<?php

namespace Tests\Unit\Math\Operations;

use App\Math\Contracts\OperationContract;
use App\Math\Contracts\OperationResultContract;
use App\Math\Operations\Division;
use App\Math\Operations\OperationResult;
use Tests\TestCase;

class DivisionTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var OperationContract $operation */
        $operation = new Division(new OperationResult());

        $this->assertInstanceOf(Division::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }

    /** @test */
    public function testCorrectResults()
    {
        /** @var OperationContract $operation */
        $operation = new Division(new OperationResult());

        // act
        $result = $operation->execute([
            [1, 2],
            [1, 10],
            [3, 1],
            [30, 10],
        ]);

        // assert
        $this->assertInstanceOf(OperationResultContract::class, $result);
        $this->assertEquals([
            [1, 2, 0.5],
            [1, 10, 0.1],
            [3, 1, 3],
            [30, 10, 3],
        ], $result->data());
        $this->assertEquals([], $result->errors());
    }
}
