<?php

namespace Tests\Unit\Math\Operations;

use App\Math\Contracts\OperationContract;
use App\Math\Contracts\OperationResultContract;
use App\Math\Operations\Minus;
use App\Math\Operations\OperationResult;
use Tests\TestCase;

class MinusTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var OperationContract $operation */
        $operation = new Minus(new OperationResult());

        $this->assertInstanceOf(Minus::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }

    /** @test */
    public function testCorrectResults()
    {
        /** @var OperationContract $operation */
        $operation = new Minus(new OperationResult());

        // act
        $result = $operation->execute([
            [1, -1],
            [1, -2],
            [1, 0],
            [2.2, 1.1],
        ]);

        // assert
        $this->assertInstanceOf(OperationResultContract::class, $result);
        $this->assertEquals([
            [1, -1, 2],
            [1, -2, 3],
            [1, 0, 1],
            [2.2, 1.1, 1.1],
        ], $result->data());
        $this->assertEquals([], $result->errors());
    }
}
