<?php

namespace Tests\Unit\Math\Operations;

use App\Math\Contracts\OperationContract;
use App\Math\Contracts\OperationResultContract;
use App\Math\Operations\Multiply;
use App\Math\Operations\OperationResult;
use Tests\TestCase;

class MultiplyTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var OperationContract $operation */
        $operation = new Multiply(new OperationResult());

        $this->assertInstanceOf(Multiply::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }

    /** @test */
    public function testCorrectResults()
    {
        /** @var OperationContract $operation */
        $operation = new Multiply(new OperationResult());

        // act
        $result = $operation->execute([
            [1, 2],
            [1, 1],
            [-1, -1],
            [1, 0],
            [1.1, 2],
        ]);

        // assert
        $this->assertInstanceOf(OperationResultContract::class, $result);
        $this->assertEquals([
            [1, 2, 2],
            [1, 1, 1],
            [-1, -1, 1],
            [1, 0, 0],
            [1.1, 2, 2.2],
        ], $result->data());
        $this->assertEquals([], $result->errors());
    }
}
