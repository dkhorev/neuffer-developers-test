<?php

namespace Tests\Unit\Math\Operations;

use App\Math\Contracts\OperationResultContract;
use App\Math\Operations\OperationResult;
use Tests\TestCase;

class OperationResultTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var OperationResultContract $result */
        $result = new OperationResult();

        $this->assertInstanceOf(OperationResult::class, $result);
        $this->assertInstanceOf(OperationResultContract::class, $result);
    }

    /** @test */
    public function testSaveAndGetData()
    {
        /** @var OperationResultContract $result */
        $result = new OperationResult();

        // act
        $result->addData([1,2,3]);
        $result->addData([1,1,2]);

        // assert
        $this->assertEquals([
            [1,2,3],
            [1,1,2]
        ], $result->data());
        $this->assertEquals([], $result->errors());
    }

    /** @test */
    public function testSaveAndGetErrors()
    {
        /** @var OperationResultContract $result */
        $result = new OperationResult();

        // act
        $result->addError([1,2,3]);
        $result->addError([1,1,2]);

        // assert
        $this->assertEquals([
            [1,2,3],
            [1,1,2]
        ], $result->errors());
        $this->assertEquals([], $result->data());
    }
}
