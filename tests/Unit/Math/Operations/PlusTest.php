<?php

namespace Tests\Unit\Math\Operations;

use App\Math\Contracts\OperationContract;
use App\Math\Contracts\OperationResultContract;
use App\Math\Operations\OperationResult;
use App\Math\Operations\Plus;
use Tests\TestCase;

class PlusTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var OperationContract $operation */
        $operation = new Plus(new OperationResult());

        $this->assertInstanceOf(Plus::class, $operation);
        $this->assertInstanceOf(OperationContract::class, $operation);
    }

    /** @test */
    public function testCorrectResults()
    {
        /** @var OperationContract $operation */
        $operation = new Plus(new OperationResult());

        // act
        $result = $operation->execute([
            [1, 2],
            [1, -1],
            [1, 2],
            [1.1, 2.2],
            [10, 0],
            [0, 10],
        ]);

        // assert
        $this->assertInstanceOf(OperationResultContract::class, $result);
        $this->assertEquals([
            [1, 2, 3],
            [1, -1, 0],
            [1, 2, 3],
            [1.1, 2.2, 3.3],
            [10, 0, 10],
            [0, 10, 10],
        ], $result->data());
        $this->assertEquals([], $result->errors());
    }
}
