<?php

namespace Tests\Unit\Math\Rules\Result;

use App\Math\Contracts\InputRuleContract;
use App\Math\Contracts\ResultRuleContract;
use App\Math\Rules\Result\NoNegative;
use Tests\TestCase;

class NoNegativeTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var InputRuleContract $rule */
        $rule = new NoNegative();

        $this->assertInstanceOf(NoNegative::class, $rule);
        $this->assertInstanceOf(ResultRuleContract::class, $rule);
    }

    /** @test */
    public function testPasses()
    {
        /** @var ResultRuleContract $rule */
        $rule = new NoNegative();

        $this->assertTrue($rule->passes(1));
        $this->assertTrue($rule->passes(3));
        $this->assertTrue($rule->passes(3.3));
        $this->assertTrue($rule->passes(0));
        $this->assertTrue($rule->passes(-0));
    }

    /** @test */
    public function testFails()
    {
        /** @var ResultRuleContract $rule */
        $rule = new NoNegative();

        $this->assertFalse($rule->passes(-1));
        $this->assertFalse($rule->passes(-3));
        $this->assertFalse($rule->passes(-3.3));
        $this->assertFalse($rule->passes(-0.00003));
    }
}
