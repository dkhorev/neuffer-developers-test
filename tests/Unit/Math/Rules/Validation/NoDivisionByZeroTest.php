<?php

namespace Tests\Unit\Math\Rules\Validation;

use App\Math\Contracts\InputRuleContract;
use App\Math\Rules\Validation\NoDivisionByZero;
use Tests\TestCase;

class NoDivisionByZeroTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var InputRuleContract $rule */
        $rule = new NoDivisionByZero();

        $this->assertInstanceOf(NoDivisionByZero::class, $rule);
        $this->assertInstanceOf(InputRuleContract::class, $rule);
    }

    /** @test */
    public function testPasses()
    {
        /** @var InputRuleContract $rule */
        $rule = new NoDivisionByZero();

        $this->assertTrue($rule->passes([1,2]));
        $this->assertTrue($rule->passes([0,2]));
        $this->assertTrue($rule->passes([1,-1]));
    }

    /** @test */
    public function testFails()
    {
        /** @var InputRuleContract $rule */
        $rule = new NoDivisionByZero();

        $this->assertFalse($rule->passes([1,0]));
        $this->assertFalse($rule->passes([0,0]));
        $this->assertFalse($rule->passes([1,'dog']));
        $this->assertFalse($rule->passes([1,null]));
        $this->assertFalse($rule->passes([1,false]));
    }
}
