<?php

namespace Tests\Unit\Math\Rules\Validation;

use App\Math\Contracts\InputRuleContract;
use App\Math\Rules\Validation\ValidArguments;
use Tests\TestCase;

class ValidArgumentsTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var InputRuleContract $rule */
        $rule = new ValidArguments();

        $this->assertInstanceOf(ValidArguments::class, $rule);
        $this->assertInstanceOf(InputRuleContract::class, $rule);
    }

    /** @test */
    public function testPasses()
    {
        /** @var InputRuleContract $rule */
        $rule = new ValidArguments();

        $this->assertTrue($rule->passes([1, 2]));
        $this->assertTrue($rule->passes([0, 2]));
        $this->assertTrue($rule->passes([1, -1]));
        $this->assertTrue($rule->passes([1.1, -1]));
        $this->assertTrue($rule->passes([1.1, -1.1]));
        $this->assertTrue($rule->passes([10, -1.1]));
        $this->assertTrue($rule->passes([0, 0]));
    }

    /** @test */
    public function testFails()
    {
        /** @var InputRuleContract $rule */
        $rule = new ValidArguments();

        $this->assertFalse($rule->passes([1, 'dog']));
        $this->assertFalse($rule->passes([1, null]));
        $this->assertFalse($rule->passes([1, false]));
        $this->assertFalse($rule->passes([false, false]));
        $this->assertFalse($rule->passes([null, false]));
        $this->assertFalse($rule->passes([null, null]));
        $this->assertFalse($rule->passes([null, '']));
        $this->assertFalse($rule->passes([null, 10]));
    }
}
